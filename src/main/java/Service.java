import com.google.gson.Gson;
import com.lambdaworks.redis.RedisClient;
import com.lambdaworks.redis.api.StatefulRedisConnection;
import com.lambdaworks.redis.api.sync.RedisCommands;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import org.apache.commons.codec.digest.DigestUtils;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.soap.Node;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import static spark.Spark.port;
import static spark.Spark.post;

/**
 * Created by node-iboy on 17/05/17.
 */
public class Service {

    private static final String payment="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://ws.apache.org/axis2\">\n" +
            "   <SOAP-ENV:Body>\n" +
            "      <ns1:pospayPayment>\n" +
            "         <ns1:ID_PETUGAS>%s</ns1:ID_PETUGAS>\n" +
            "         <ns1:PASS_PETUGAS>%s</ns1:PASS_PETUGAS>\n" +
            "         <ns1:IDTIPEREF>%s</ns1:IDTIPEREF>\n" +
            "         <ns1:DATA>%s|%s|%s:%s:%s:%s:%s|20||%s:%s, idreff : %s|</ns1:DATA>\n" +
            "         <ns1:SIGNATURE>%s</ns1:SIGNATURE>\n" +
            "         <ns1:PASSCODE_DATA>%s|%s|%s</ns1:PASSCODE_DATA>\n" +
            "         <ns1:PASSCODE_SIGN>%s</ns1:PASSCODE_SIGN>\n" +
            "         <ns1:OPTIONAL_DATA>04~!%s~!%s~!%s~!~!~!%s~!%s~!~!~!%s~!~!%s</ns1:OPTIONAL_DATA>\n" +
            "      </ns1:pospayPayment>\n" +
            "   </SOAP-ENV:Body>\n" +
            "</SOAP-ENV:Envelope>";

    private static final String inquiry="<SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://ws.apache.org/axis2\">" +
            "<SOAP-ENV:Body> " +
            "<ns1:pospayInquiry>" +
            "<ns1:DATA>03~!%s~!%s~!%s~!~!~!%s~!0~!~!~!~!~!</ns1:DATA>" +
            "</ns1:pospayInquiry>" +
            "</SOAP-ENV:Body>" +
            "</SOAP-ENV:Envelope>";

    public  static  void main(String[] args){
        RedisClient redisClient = RedisClient.create("redis://localhost:6379/0");
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisCommands<String, String> syncCommands = connection.sync();
        port(7001);
        System.out.print("Start spark with port: 7001");
        post("/inquiry", (req, res) -> {
            Map<String, String> map;

            String amounResponse ="";
            String amountFee = "";
            String reference = "";
            String display = "";
            Integer amountPayment = 0;
            String status="";
            try{
                map = new Gson().fromJson(req.body(), Map.class);
            }catch (Exception e){
                res.status(400);
                return "{bad request}";
            }
            String code = syncCommands.get(String.valueOf(map.get("provider"))+":"+String.valueOf(map.get("amount")));
            if(code==null || code.isEmpty()){
                throw new Exception("Voucher Code Not Found :"+String.valueOf(map.get("provider"))+":"+String.valueOf(map.get("amount")));
            }
            String[] codeSplitted =code.split(":");
            try {
                String provider =String.valueOf(map.get("provider"));
                String userNumber ="";
                if(provider.equals("pln") || provider.equals("tokenpln")){
                    userNumber = String.valueOf(map.get("phone"));
                }else{
                    userNumber = codeSplitted[0].concat("|").concat(String.valueOf(map.get("phone")));
                }

                String body = String.format(inquiry,String.valueOf(map.get("account")),codeSplitted[1],userNumber,String.valueOf(map.get("amount")));
                HttpResponse<String> jsonResponse = Unirest.post(String.valueOf(map.get("url")).concat("/services/PostalMobileTransaction2.PostalMobileTransaction2HttpSoap11Endpoint/"))
                        .body(body).asString();

                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new StringReader(jsonResponse.getBody())));
                org.w3c.dom.Node n = document.getFirstChild();
                NodeList nl = n.getChildNodes();
                org.w3c.dom.Node an,an2,an3;

                Map<String,String>  tagNode = new HashMap<>();
                for (int i=0; i < nl.getLength(); i++) {
                    an = nl.item(i);
                    if(an.getNodeType()== Node.ELEMENT_NODE) {
                        NodeList nl2 = an.getChildNodes();
                        for(int i2=0; i2<nl2.getLength(); i2++) {
                            an2 = nl2.item(i2);
                            NodeList nl3 = an2.getChildNodes();
                            for(int i3=0; i3<nl3.getLength();i3++){
                                an3 = nl3.item(i3);
                                org.w3c.dom.Node sibling = an3.getFirstChild();
                                //input first child node
                                if(sibling!=null)tagNode.put(sibling.getNodeName(),sibling.getTextContent());
                                while (sibling != null) {
                                    try{
                                        //iterate next child
                                        sibling = sibling.getNextSibling();
                                        tagNode.put( sibling.getNodeName(),sibling.getTextContent());
                                    }catch (NullPointerException e){
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }


                String response = tagNode.get("#text");
                String[] responseSplit =response.split("~!");
                status = responseSplit[5];
                if(!status.equals("SUKSES")){
                    res.status(400);
                    return "{failed inquiry : "+status+"}";
                }
                amounResponse =responseSplit[6];
                amountFee = responseSplit[7];
                reference = responseSplit[10];
                display = responseSplit[12].replace("~#","");
                amountPayment = Integer.parseInt(responseSplit[6])+Integer.parseInt(responseSplit[7]);

                /*return "{\"reference\": \"" +responseSplit[10]+"\"},\"display\": \"" + responseSplit[12] + "\"}";*/
                /*return jsonResponse.getBody();*/
            } catch (Exception e) {
                res.status(500);
                return "{error inquiry}";
            }

            try {
                String data ="%s|%s|%s:%s:%s:%s:%s|20||%s:%s, idreff : %s|";
                String dataFormatted = String.format(data,String.valueOf(map.get("account")),String.valueOf(amountPayment),codeSplitted[1],reference,
                        String.valueOf(map.get("phone")),String.valueOf(map.get("param1")),String.valueOf(map.get("cif")), codeSplitted[2], String.valueOf(map.get("phone")),reference);
                String passcodeData = String.valueOf(map.get("username")).concat("|"+String.valueOf(map.get("password"))).concat("|"+String.valueOf(map.get("passcode")));
                String signature=String.valueOf(map.get("idPetugas")).concat(String.valueOf(map.get("passPetugas"))).concat(String.valueOf(map.get("idTiPeref")));
                String body = String.format(payment,String.valueOf(map.get("idPetugas")),String.valueOf(map.get("passPetugas")),String.valueOf(map.get("idTiPeref")),
                        String.valueOf(map.get("account")),String.valueOf(amountPayment),codeSplitted[1],reference,String.valueOf(map.get("phone")),
                        String.valueOf(map.get("param1")),String.valueOf(map.get("cif")),codeSplitted[2], String.valueOf(map.get("phone")),reference,
                        sha1converter(signature.concat(dataFormatted)),
                        String.valueOf(map.get("username")),String.valueOf(map.get("password")),String.valueOf(map.get("passcode")),
                        sha1converter(signature.concat(passcodeData)),
                        String.valueOf(map.get("account")),codeSplitted[1],String.valueOf(map.get("phone")),amounResponse,amountFee,reference,display);
                HttpResponse<String> jsonResponse = Unirest.post(String.valueOf(map.get("url")).concat("/services/PostalMobileTransaction2.PostalMobileTransaction2HttpSoap11Endpoint/"))
                        .body(body).asString();


                DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
                DocumentBuilder builder = factory.newDocumentBuilder();
                Document document = builder.parse(new InputSource(new StringReader(jsonResponse.getBody())));
                org.w3c.dom.Node n = document.getFirstChild();
                NodeList nl = n.getChildNodes();
                org.w3c.dom.Node an,an2,an3;

                Map<String,String>  tagNode = new HashMap<>();
                for (int i=0; i < nl.getLength(); i++) {
                    an = nl.item(i);
                    if(an.getNodeType()== Node.ELEMENT_NODE) {
                        NodeList nl2 = an.getChildNodes();
                        for(int i2=0; i2<nl2.getLength(); i2++) {
                            an2 = nl2.item(i2);
                            NodeList nl3 = an2.getChildNodes();
                            for(int i3=0; i3<nl3.getLength();i3++){
                                an3 = nl3.item(i3);
                                org.w3c.dom.Node sibling = an3.getFirstChild();
                                //input first child node
                                if(sibling!=null)tagNode.put(sibling.getNodeName(),sibling.getTextContent());
                                while (sibling != null) {
                                    try{
                                        //iterate next child
                                        sibling = sibling.getNextSibling();
                                        tagNode.put( sibling.getNodeName(),sibling.getTextContent());
                                    }catch (NullPointerException e){
                                        continue;
                                    }
                                }
                            }
                        }
                    }
                }


                String response = tagNode.get("#text");
                String[] responseSplit =response.split("~!");
                status = responseSplit[5];
                if(!status.equals("SUKSES")){
                    res.status(400);
                    return "{failed payment : "+status+"}";
                }else{
                    res.status(200);
                    return jsonResponse.getBody();
                }
                /*return body;*/
            }catch (IndexOutOfBoundsException ind){
                res.status(400);
                return "{saldo tidak cukup}";
            }catch (Exception e) {
                res.status(500);
                return "{failed payment}";
            }
        });
    }

    private static String sha1converter(String param){
        return DigestUtils.sha1Hex(param);
    }
}
